package backind.backind.Utils;

public interface ListHomestayListener {
    void onSortingButtonClicked(int id_city, String keysearch);
    void onFilterButtonClicked(int id_city, int minprizes, int maxprizes, int avg_review);
    void onResetButtonClicked(int id_city, int minprizes, int maxprizes, int avg_review, String reset);
}

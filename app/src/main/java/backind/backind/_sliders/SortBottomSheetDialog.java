package backind.backind._sliders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import backind.backind.Adapter.TourismAdapter;
import backind.backind.Constant;
import backind.backind.Model.BusinessData;
import backind.backind.Model.Review;
import backind.backind.R;
import backind.backind.Response.BusinessResponse;
import backind.backind.Service.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SortBottomSheetDialog extends BottomSheetDialogFragment {
    private BottomSheetListener bottomSheetListener;
    Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.sort_bottom_sheet_layout, container, false);

        TextView lowest = v.findViewById(R.id.lowest);
        TextView highets = v.findViewById(R.id.highest);
        TextView review = v.findViewById(R.id.review);

        lowest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lowprice = "lowprice";
                bottomSheetListener.onButtonSortClicked("lowprice");
                dismiss();
            }
        });
        highets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetListener.onButtonSortClicked("highprice");
                dismiss();
            }
        });
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetListener.onButtonSortClicked("review");
                dismiss();
            }
        });

        return v;
    }

    public interface BottomSheetListener{
        void onButtonSortClicked(String text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            bottomSheetListener = (BottomSheetListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()
            + "must implement BottomSheetListener");
        }
    }
}

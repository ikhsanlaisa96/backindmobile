package backind.backind._sliders;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.text.NumberFormat;
import java.util.Locale;

import backind.backind.R;

public class filterBottomSheetDialog extends BottomSheetDialogFragment {
    Button sad, netral, smile, happy, done, reset;
    EditText minvalue, maxvalue;
    TextView review;
    String minvalues, maxvalues, reviews, resets;
    int min = 0, max=200000;
    private BottomSheetListener bottomSheetListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filter_bottom_sheet_layout, container, false);

        RangeSeekBar rangeSeekBar = v.findViewById(R.id.seek);

        RatingBar ratingBar = v.findViewById(R.id.rating_bar);
        minvalue = v.findViewById(R.id.rangemin);
        maxvalue = v.findViewById(R.id.rangemax);
        review = v.findViewById(R.id.review);
        done = v.findViewById(R.id.done);
        reset = v.findViewById(R.id.reset);
//        sad = v.findViewById(R.id.sad);
//        netral = v.findViewById(R.id.netral);
//        smile = v.findViewById(R.id.smile);
//        happy = v.findViewById(R.id.happy);
        review.setText("0");
        minvalue.setText(String.valueOf(min));
        maxvalue.setText(String.valueOf(max));
//        sad.setOnClickListener(this);
//        netral.setOnClickListener(this);
//        smile.setOnClickListener(this);
//        happy.setOnClickListener(this);


        rangeSeekBar.setRangeValues(min, max);
        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                Log.e("value", minValue + " " + maxValue);
                minvalue.setText(minValue.toString());
                maxvalue.setText(maxValue.toString());
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                review.setText(String.valueOf((int) v));
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (minvalues == null){
                    minvalue.setText("0");
                    maxvalues = maxvalue.getText().toString();
                    reviews = review.getText().toString();
                    minvalues = "0";
                }else if (reviews.equalsIgnoreCase("")) {
                    minvalues = minvalue.getText().toString();
                    maxvalues = maxvalue.getText().toString();
                    review.setText("0");
                    reviews = "0";
                }
                else if (minvalues.equalsIgnoreCase("") && maxvalues.equalsIgnoreCase("")){
                    minvalue.setText("0");
                    maxvalue.setText("0");
                    reviews = review.getText().toString();
                    minvalues = "0";
                    maxvalues = "0";
                }else {
                    minvalues = minvalue.getText().toString();
                    maxvalues = maxvalue.getText().toString();
                    reviews = review.getText().toString();
                }
                Log.d("ini lohhhhh", "ini lohhhhh" + reviews);
                bottomSheetListener.onButtonFilterClicked(minvalues, maxvalues, reviews);
                dismiss();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                minvalues = minvalue.getText().toString();
                maxvalues = maxvalue.getText().toString();
                reviews = review.getText().toString();
                resets = reset.getText().toString();
                bottomSheetListener.onResetFilterClicked(minvalues, maxvalues, reviews, resets);
                dismiss();
            }
        });

        return v;
    }

    public interface BottomSheetListener {
        void onButtonFilterClicked(String minprize, String maxprize, String review);
        void onResetFilterClicked(String minprize, String maxprize, String review, String reset);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            bottomSheetListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "must implement BottomSheetListener");
        }
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.sad:
//                review.setText("1");
//                Toast.makeText(getContext(),"Review Sad",Toast.LENGTH_LONG).show();
//                break;
//            case R.id.netral:
//                review.setText("2");
//                Toast.makeText(getContext(),"Review Netral",Toast.LENGTH_LONG).show();
//                break;
//            case R.id.smile:
//                review.setText("3");
//                Toast.makeText(getContext(),"Review Smile",Toast.LENGTH_LONG).show();
//                break;
//            case R.id.happy:
//                review.setText("4");
//                Toast.makeText(getContext(),"Review Happy",Toast.LENGTH_LONG).show();
//                break;
//        }
//    }
}

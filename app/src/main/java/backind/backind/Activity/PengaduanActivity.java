package backind.backind.Activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;

import backind.backind.Adapter.PengaduanAdapter;
import backind.backind.Constant;
import backind.backind.Model.Pengaduan;
import backind.backind.Model.PengaduanRequest;
import backind.backind.Model.Timeline;
import backind.backind.R;
import backind.backind.Service.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengaduanActivity extends AppCompatActivity {
    private FloatingActionButton fab;
    RecyclerView recyclerView;
    LinearLayout layoutEmpty;
    ArrayList<Pengaduan> pengaduan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaduan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("PENGADUAN");
        Hawk.init(this).build();
        layoutEmpty = (LinearLayout)findViewById(R.id.no_data);
        recyclerView = (RecyclerView)findViewById(R.id.list_pengaduan);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getPengaduan();

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.add_pengaduan);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PengaduanActivity.this, New_pengaduan.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPengaduan();
    }

    private void getPengaduan(){
        pengaduan = new ArrayList<>();
        final String token = "bearer" + Hawk.get(Constant.TOKEN, "");

        Api.getService().getPengaduan().enqueue(new Callback<PengaduanRequest>() {
            @Override
            public void onResponse(Call<PengaduanRequest> call, Response<PengaduanRequest> response) {
                if (response.isSuccessful()){
                    PengaduanRequest pengaduanRequest = response.body();
                    if (pengaduanRequest.getMessage().equals("success")){

                        pengaduan = pengaduanRequest.getData().getPengaduan();
                        Log.d("PENGADUAN",pengaduanRequest.getMessage());
                        PengaduanAdapter adapter = new PengaduanAdapter(getApplicationContext(), pengaduan, new PengaduanAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(Pengaduan item) {
                                Intent intent = new Intent(getApplicationContext(), DetailPengaduanActivity.class);
                                intent.putExtra("id",item.getId());
                                intent.putExtra("judul", "Ini Judul Pengaduan");
                                intent.putExtra("kategori", "Kategori "+item.getLayanan().getJenis());
                                intent.putExtra("isi", item.getIsi());
                                intent.putExtra("hasil", item.getHasil());
                                intent.putExtra("tanggal", item.getCreatedAt());
                                intent.putExtra("foto", Hawk.get(Constant.BASE_URL_PHOTO)+item.getFoto());
                                ArrayList<Timeline> timelines = item.getStatus();
                                Bundle args = new Bundle();
                                args.putSerializable("status",timelines);
                                intent.putExtra("bundle",args);
                                Log.d("timelineeee", "timelineee" + args);
                                startActivity(intent);
                            }
                        });
                        Log.d("inii lohh datanya", "inii lohh datanya" + adapter);
                        recyclerView.setAdapter(adapter);
                        if(pengaduan.isEmpty()){
                            layoutEmpty.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }
                        else {

                            layoutEmpty.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                    }
                    else{
                        Log.e("ERROR0", pengaduanRequest.getMessage());
                    }
                }
                else{
                    Log.e("ERROR1",response.toString());
                }
            }

            @Override
            public void onFailure(Call<PengaduanRequest> call, Throwable t) {
                Log.e("ERROR2",call.toString()+" "+token);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }
}

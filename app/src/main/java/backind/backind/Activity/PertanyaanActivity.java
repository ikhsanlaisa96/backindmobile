package backind.backind.Activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import backind.backind.Adapter.SurveyAdapter;
import backind.backind.Constant;
import backind.backind.Model.Opsi;
import backind.backind.Model.Request;
import backind.backind.Model.SurveyRequest;
import backind.backind.Model.pertanyaan;
import backind.backind.R;
import backind.backind.Service.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PertanyaanActivity extends AppCompatActivity {
    ArrayList<pertanyaan> pertanyaan;
    ArrayList<Opsi> opsi;
    TextView nomorTxt;
    ViewPager viewPager;
    SurveyAdapter adapter;
    ImageButton btnLeft, btnRight;
    Button btnSubmit;
    Integer transaction_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("FORM SURVEY");
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("bundle");
        transaction_id = intent.getIntExtra("transaction_id", 0);
        pertanyaan = (ArrayList<pertanyaan>) args.getSerializable("pertanyaan");
        Log.d("PERTANYAAN : ", "Ukuran : " + pertanyaan.size());
        Toast.makeText(getApplicationContext(), "transaction_id " + transaction_id, Toast.LENGTH_LONG).show();
        btnLeft = (ImageButton) findViewById(R.id.btn_left);
        btnRight = (ImageButton) findViewById(R.id.btn_right);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        nomorTxt = (TextView) findViewById(R.id.nomor);

        nomorTxt.setText("1 / " + pertanyaan.size());

        btnLeft.setVisibility(View.GONE);
        if (0 == (pertanyaan.size() - 1)) {
            btnRight.setVisibility(View.GONE);
        }

        adapter = new SurveyAdapter(getApplicationContext(), pertanyaan, new SurveyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(backind.backind.Model.pertanyaan item) {

            }
        });

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int i) {
                nomorTxt.setText((i + 1) + " / " + pertanyaan.size());
                if (i == 0) {
                    btnLeft.setVisibility(View.GONE);
                } else {
                    btnLeft.setVisibility(View.VISIBLE);
                }
                if (i == (pertanyaan.size() - 1)) {
                    btnRight.setVisibility(View.GONE);
                } else {
                    btnRight.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }

    public void toRight(View view) {
        if (viewPager.getCurrentItem() < pertanyaan.size())
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
    }

    public void toLeft(View view) {
        if (viewPager.getCurrentItem() != 0)
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
    }

    public void submitSurvey(View view) {
        btnSubmit.setEnabled(false);
        int i = 0;
        ArrayList<Integer> checkAnswer = adapter.getCheckedIds();
        List<Integer> idPertanyaan = new ArrayList<>();
        List<String> jawaban = new ArrayList<>();
        for (pertanyaan p : pertanyaan) {
            idPertanyaan.add(p.getId());
            if (checkAnswer.get(i) == null) {
                Toast.makeText(this, "Harap pilih opsi jawaban pertanyaan " + (i + 1) + " !", Toast.LENGTH_SHORT).show();
                viewPager.setCurrentItem(i);
                btnSubmit.setEnabled(true);
                return;
            } else {
                jawaban.add(checkAnswer.get(i) + "");
            }
            i++;
        }
        String token = "bearer"+ Hawk.get(Constant.TOKEN,"");
        Call<Request> submitsurvey = Api.getService().submitSurvey(token, transaction_id, new Gson().toJson(idPertanyaan), new Gson().toJson(jawaban));
        Log.d("PERTANYAAN", new Gson().toJson(idPertanyaan));
        Log.d("JAWABAN", new Gson().toJson(jawaban));
        submitsurvey.enqueue(new Callback<Request>() {
            @Override
            public void onResponse(Call<Request> call, Response<Request> response) {
                if(response.isSuccessful()){
                    Request res = response.body();
                    if(res.getError() == false){
                        Toast.makeText(PertanyaanActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                    else{
                        Toast.makeText(PertanyaanActivity.this, res.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                else{
                    Log.e("ERROR1",response.toString());
                }
                btnSubmit.setEnabled(true);
            }

            @Override
            public void onFailure(Call<Request> call, Throwable t) {
                Log.e("ERROR2",t.getMessage());
            }
        });
    }
}

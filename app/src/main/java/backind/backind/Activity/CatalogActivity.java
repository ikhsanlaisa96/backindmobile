package backind.backind.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import backind.backind.Adapter.CatalogAdapter;
import backind.backind.Model.Catalog;
import backind.backind.R;
import backind.backind.Service.CatalogAPi;
import backind.backind.Service.CatalogInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatalogActivity extends AppCompatActivity {

    Bundle bundle;
    RecyclerView recyclerView;
    CatalogAPi mApiInterface;
    CatalogAdapter catalogAdapter;
    private Gson gson;

    ArrayList<Catalog.Group> catalogs = new ArrayList<Catalog.Group>();

    String url = "http://beta.backind.com:8081/api/catalog/groups?amount=";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        recyclerView = findViewById(R.id.rv);

        GsonBuilder gsonBuilder = new GsonBuilder().serializeNulls();
        gson = gsonBuilder.create();
        bundle = getIntent().getExtras();
//        mApiInterface = CatalogAPi.getClient().create(CatalogInterface.class);

        getCatalog(String.valueOf(bundle.getString("budget")),String.valueOf(bundle.getString("loosy")));
//        getCatalog(String.valueOf(bundle.getString("budget")));

        catalogAdapter = new CatalogAdapter(this, catalogs);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(catalogAdapter);


    }
    //kondisi tambah loosy
    public void getCatalog(String nominal, String loosy) {
        AndroidNetworking.get(url + nominal + loosy )
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        setIsLoading(false);
                        GsonBuilder gsonBuilder = new GsonBuilder().serializeNulls();
                        gson = gsonBuilder.create();
                        Catalog courseRespMdl = gson.fromJson(String.valueOf(response), Catalog.class);
                        if (courseRespMdl.getGroups() != null) {
                            if (courseRespMdl.getGroups().size() != 0) {
                                updateCourseTrending(courseRespMdl);
                            } else {
                                Log.e("login", "onResponse: " + "no courses found");
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
//                        setIsLoading(false);
//                        handleErrorNetwork(anError);
                    }
                });
    }

    private void updateCourseTrending(Catalog courseRespMdl) {
        Catalog.Group group;
        catalogs.clear();
        for (int i = 0; i < courseRespMdl.getGroups().size(); i++){
            group =courseRespMdl.getGroups().get(i);
            catalogs.add(group);
        }
        catalogAdapter.notifyDataSetChanged();
    }


}

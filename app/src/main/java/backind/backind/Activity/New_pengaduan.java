package backind.backind.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import backind.backind.Constant;
import backind.backind.Model.AddPengaduanRequest;
import backind.backind.Model.Layanan;
import backind.backind.Model.LayananRequest;
import backind.backind.Model.User;
import backind.backind.R;
import backind.backind.Service.Api;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class New_pengaduan extends AppCompatActivity {

    private ClipData.Item takePictureButton;
    private ImageView imageView;
    private Button btnSubmit;
    private EditText editLaporan;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private static final int REQUEST_GET_SINGLE_FILE = 101;
    private Spinner spintoyou;
    private String isi;
    private int idUser, idJenis;
    String imagePath = "";
    Uri uri;
    private User user=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pengaduan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Hawk.init(this).build();
        imageView = findViewById(R.id.change_image);
        btnSubmit = findViewById(R.id.btn_submit);
        editLaporan = findViewById(R.id.edit_laporan);
        getSupportActionBar().setTitle("BUAT PENGADUAN");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

        spintoyou = findViewById(R.id.list_layanan);
        Hawk.init(this).build();
        String tkn = Hawk.get(Constant.TOKEN,"");
        tkn = tkn.replaceFirst("Bearer ", "bearer");
        String token = tkn;
        final List<String> jenisPengaduan = new ArrayList<>();
        Api.getService().getLayanan().enqueue(new Callback<LayananRequest>() {
            @Override
            public void onResponse(Call<LayananRequest> call, Response<LayananRequest> response) {
                if (response.isSuccessful()) {
                    LayananRequest layananRequest = response.body();
                    if (layananRequest.getMessage().equals("success")) {
                        final ArrayList<Layanan> layanan = layananRequest.getData().getLayanan();
                        Log.d("PENGADUAN", layananRequest.toString());
                        for (Layanan l : layanan) {
                            jenisPengaduan.add(l.getJenis());
                        }
                        idJenis = layanan.get(0).getId();
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.spinner_item, jenisPengaduan);
                        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
                        // mengeset Array Adapter tersebut ke Spinner
                        spintoyou.setAdapter(adapter);
                        // mengeset listener untuk mengetahui saat item dipilih
                        spintoyou.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                                idJenis = layanan.get(i).getId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } else {
                        Log.e("ERROR0", layananRequest.toString());
                    }
                } else {
                    Log.e("ERROR1", response.toString());
                }
            }

            @Override
            public void onFailure(Call<LayananRequest> call, Throwable t) {
                Log.e("ERROR2", call.toString());
            }
        });
    }

    public void submit() {
        btnSubmit.setEnabled(false);
        isi = editLaporan.getText().toString();
        user = Hawk.get(Constant.DataLocal.dataUser);
        idUser = user.getIdUser();
        if (isi.equals("")) {
            Toast.makeText(this, "Isi pengaduan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("ISI", isi);
            MultipartBody.Part body;
            if (imagePath != "") {
                File file = new File(imagePath);
                Log.d("FOTO2", file.getAbsolutePath());
                // create RequestBody instance from file
//                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(getContentResolver().getType(uri)),
                                file
                        );
                body = MultipartBody.Part.createFormData("foto", file.getName(), requestFile);
            } else {
                body = null;
            }
            String tkn = Hawk.get(Constant.TOKEN,"");
            tkn = tkn.replaceFirst("Bearer ", "bearer");
            String token = tkn;
            RequestBody isiBody = RequestBody.create(MediaType.parse("text/plain"), isi);

            Api.getService().addPengaduan(idJenis, isiBody, body).enqueue(new Callback<AddPengaduanRequest>() {
                @Override
                public void onResponse(Call<AddPengaduanRequest> call, Response<AddPengaduanRequest> response) {
                    if(response.isSuccessful()){
                        AddPengaduanRequest data = response.body();
                        if(data.getError() == false){
                            Toast.makeText(New_pengaduan.this, "Berhasil membuat laporan!", Toast.LENGTH_SHORT).show();
                            New_pengaduan.super.onBackPressed();
                        }
                        else{
                            Toast.makeText(New_pengaduan.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Log.e("ERROR1",response.toString());
                    }
                    btnSubmit.setEnabled(true);
                }

                @Override
                public void onFailure(Call<AddPengaduanRequest> call, Throwable t) {
                    Log.e("ERROR2",t.getMessage());
                    btnSubmit.setEnabled(true);
                }
            });
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.kamera, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home :
                onBackPressed();
                break;
            case R.id.camera_atas:
                openGalleryIntent();
                break;

        }
        return true;
    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE
        );
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(pictureIntent,
                    REQUEST_CAPTURE_IMAGE);
        }
    }

    private void openGalleryIntent() {
        if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
            // do your stuff..

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GET_SINGLE_FILE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAPTURE_IMAGE) {
                if (data != null && data.getExtras() != null) {
                    Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                    imageView.setImageBitmap(imageBitmap);
                }
            } else if (requestCode == REQUEST_GET_SINGLE_FILE) {
                uri = data.getData();
                imagePath = getPathFromURI(getApplicationContext(), uri);
                //Log.d("FOTO",imagePath);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    // Log.d(TAG, String.valueOf(bitmap));
                    imageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{permission},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // do your stuff
                } else {
                    Toast.makeText(getApplicationContext(), "GET_ACCOUNTS Denied",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions,
                        grantResults);
        }
    }

    public static String getPathFromURI(Context context, Uri uri) {
        String filePath = "";
        String fileId = DocumentsContract.getDocumentId(uri);
        // Split at colon, use second item in the array
        String id = fileId.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        String selector = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, selector, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }
}

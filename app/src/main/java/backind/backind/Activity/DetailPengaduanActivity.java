package backind.backind.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import backind.backind.Adapter.StatusPengaduanAdapter;
import backind.backind.Adapter.TimelinesAdapter;
import backind.backind.Model.Pengaduan;
import backind.backind.Model.PengaduanRequest;
import backind.backind.Model.Timeline;
import backind.backind.Model.TimelineRequest;
import backind.backind.Model.Timelines;
import backind.backind.R;
import backind.backind.Response.BaseResponse;
import backind.backind.Service.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPengaduanActivity extends AppCompatActivity {

    ImageView imageView;
    TextView judulTxt, isiTxt, waktuTxt, viewDetail, kategoriTxt, noTxt, statusTxt, assignee;
    boolean isImageFitToScreen = false;
    private RadioButton radiobt1, radiobt2, radiobt3;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    RecyclerView recyclerView;
    ArrayList<Timelines> timelines;
    Integer id_pengaduan;
    EditText message;
    Button sendButton;
    String notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hawk.init(this).build();
        setContentView(R.layout.activity_detail_pengaduan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("DETAIL PENGADUAN");
        imageView = (ImageView) findViewById(R.id.pengaduan_pict);
        judulTxt = (TextView) findViewById(R.id.pengaduan_judul_detail);
        isiTxt = (TextView) findViewById(R.id.detailisi_pengaduan);
        waktuTxt = (TextView) findViewById(R.id.tanggal_pengaduan);
        kategoriTxt = (TextView) findViewById(R.id.pengaduan_kategori_detail);
        statusTxt = (TextView) findViewById(R.id.pengaduan_status);
        noTxt = (TextView) findViewById(R.id.id_pengaduan);
        message = findViewById(R.id.edittext_chatbox);
//        assignee = findViewById(R.id.assignee);

        notes = message.getText().toString();
        sendButton = findViewById(R.id.button_chatbox_send);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notes == null) {
                    Toast.makeText(getApplicationContext(), "Data tidak boleh kosong", Toast.LENGTH_LONG).show();
                } else {
                    sendMessage();
                }
            }
        });

        recyclerView = findViewById(R.id.reyclerview_message_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Intent intent = getIntent();
        final String judul, isi, waktu, foto, kategori, id, hasil;
        judul = intent.getStringExtra("judul");
        isi = intent.getStringExtra("isi");
        waktu = intent.getStringExtra("tanggal");
        foto = intent.getStringExtra("foto");
        kategori = intent.getStringExtra("kategori");
        hasil = intent.getStringExtra("hasil");
        Bundle args = intent.getBundleExtra("bundle");
        id_pengaduan = intent.getIntExtra("id", 0);
        final ArrayList<Timeline> status = (ArrayList<Timeline>) args.getSerializable("status");
//        final ArrayList<Timeline> user = (ArrayList<Timeline>) args.getSerializable("status");
        Log.d("STATUS", "Size : " + status.size());
        Timeline lastStatus = null;
        for (Timeline st : status) {
            Log.d("STATUS : ", st.getStatus() + " " + st.getCreatedAt());
            if (st.getCreatedAt() != null)
                lastStatus = st;
        }
        statusTxt.setText(lastStatus.getStatus());
        if (lastStatus.getStatus().equalsIgnoreCase("NEW")) {
            statusTxt.setBackgroundResource(R.drawable.rounded_status_submit);
//            assignee.setText(lastStatus.getName());
        } else if (lastStatus.getStatus().equalsIgnoreCase("ASSIGNED")) {
            statusTxt.setBackgroundResource(R.drawable.rounded_status_progress);
//            assignee.setText(lastStatus.getName());
        } else {
            statusTxt.setBackgroundResource(R.drawable.rounded_status_selesai);
            message.setEnabled(false);
            sendButton.setEnabled(false);
//            assignee.setText(lastStatus.getName());
        }

//        assignee.setText();
        id = intent.getIntExtra("id", 0) + "";
        Picasso.get()
                .load(foto)
                .into(imageView);
        judulTxt.setText(judul);
        isiTxt.setText(isi);
        waktuTxt.setText(waktu);
        kategoriTxt.setText(kategori);
        noTxt.setText(id);
        viewDetail = (TextView) findViewById(R.id.view_detail);
        viewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new AlertDialog.Builder(DetailPengaduanActivity.this);
                inflater = getLayoutInflater();
                dialogView = inflater.inflate(R.layout.activity_status_pengaduan, null);
                TextView feedback = dialogView.findViewById(R.id.feedback);
                TextView isiFeedback = dialogView.findViewById(R.id.isi_feedback);
                if (hasil == null || hasil.equals("")) {
                    feedback.setVisibility(View.GONE);
                    isiFeedback.setVisibility(View.GONE);
                } else {
                    isiFeedback.setText(hasil);
                }

                RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.progres_pengaduan_item);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                StatusPengaduanAdapter adapter = new StatusPengaduanAdapter(getApplicationContext(), status, new StatusPengaduanAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Timeline item) {

                    }
                });
                recyclerView.setAdapter(adapter);
                dialog.setView(dialogView);
                dialog.setCancelable(true);
                dialog.show();
            }
        });

        getTimeline();
    }

    private void sendMessage() {
        if (message.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Pesan Tidak Boleh Kosong", Toast.LENGTH_LONG).show();
        } else {
            Api.getService().sendMessage(id_pengaduan, message.getText().toString()).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Berhasil Mengirim Pesan", Toast.LENGTH_LONG).show();
                        message.setText(null);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {

                }
            });
        }
    }

    private void getTimeline() {
        timelines = new ArrayList<>();
        Api.getService().getTimeline(id_pengaduan).enqueue(new Callback<TimelineRequest>() {
            @Override
            public void onResponse(Call<TimelineRequest> call, Response<TimelineRequest> response) {
                TimelineRequest timelineRequest = response.body();

                if (timelineRequest.getMessage().equals("success")) {
                    timelines = timelineRequest.getData().getTimeliness();
                    TimelinesAdapter timelinesAdapter = new TimelinesAdapter(getApplicationContext(), timelines);
                    Log.d("inii lohh datanya", "inii lohh datanya" + timelinesAdapter);
                    recyclerView.setAdapter(timelinesAdapter);
                }

            }

            @Override
            public void onFailure(Call<TimelineRequest> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }
}

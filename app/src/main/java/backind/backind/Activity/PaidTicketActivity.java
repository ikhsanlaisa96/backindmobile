package backind.backind.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;

import backind.backind.Constant;
import backind.backind.Model.Opsi;
import backind.backind.Model.Request;
import backind.backind.Model.Survey;
import backind.backind.Model.SurveyRequest;
import backind.backind.Model.UserReview;
import backind.backind.Model.UserReviewRequest;
import backind.backind.Model.pertanyaan;
import backind.backind.R;
import backind.backind.Service.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaidTicketActivity extends AppCompatActivity {

    TextView name_user, name_tourism, name_homestay, totals, jumlah_orang, date_tourism, date_homestay, id_booking, txt_homestay, txt_tourism;
    String duedate, tagihan, code, homestay, tourism, check_in, check_out, date_tiket, user, jumlah;
    //    Integer id_menu_tourism, id_menu_homestay ;
    Integer id_menu;
    View view1, view2;
    Button bSurvey;
    int status_pay, id_transaksi;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid_ticket);

        try {
            id_transaksi = getIntent().getIntExtra("id_transaksi", 0);
            duedate = getIntent().getStringExtra("duedate");
            tagihan = getIntent().getStringExtra("jumlah_tagihan");
            code = getIntent().getStringExtra("eticket");
            status_pay = getIntent().getIntExtra("status", 0);
            jumlah = getIntent().getStringExtra("jumlah_orang");
            homestay = getIntent().getStringExtra("name_homestay");
            user = getIntent().getStringExtra("name_user");
            tourism = getIntent().getStringExtra("name_tourism");
            check_in = getIntent().getStringExtra("check_in");
            check_out = getIntent().getStringExtra("check_out");
            date_tiket = getIntent().getStringExtra("date_tiket");
            if (getIntent().getIntExtra("id_menu_tourism", 0) == 1) {
                id_menu = getIntent().getIntExtra("id_menu_tourism", 0);
            } else {
                id_menu = getIntent().getIntExtra("id_menu_homestay", 0);
            }
        } catch (Exception e) {

        }

//        Toast.makeText(getApplicationContext(), "ini name tourismnya", )
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //status bar
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorWhite));

        //action bar
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#ffffff"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setElevation(0);

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.colorHitam), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        id_booking = findViewById(R.id.id_booking);
        name_user = findViewById(R.id.name_user);
        date_tourism = findViewById(R.id.date_tourism);
        date_homestay = findViewById(R.id.date_homestay);
        name_homestay = findViewById(R.id.name_homestay);
        name_tourism = findViewById(R.id.name_tourism);
        name_homestay = findViewById(R.id.name_homestay);
        totals = findViewById(R.id.totals);
        jumlah_orang = findViewById(R.id.jumlah_orang);
        txt_homestay = findViewById(R.id.txt_homestay);
        txt_tourism = findViewById(R.id.txt_tourism);
        view1 = findViewById(R.id.ghomestay);
        view2 = findViewById(R.id.gtourism);
        bSurvey = findViewById(R.id.surveybutton);
        String n = String.valueOf(jumlah);
        name_user.setText(user);
        id_booking.setText(code);
        jumlah_orang.setText(jumlah);
        //Toast.makeText(this, "homestay "+homestay, Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, "tourism "+tourism, Toast.LENGTH_SHORT).show();

        jumlah_orang.setText(n);
        totals.setText("Rp " + tagihan + ",-");

        if (tourism != null) {
            name_tourism.setText(tourism);
            date_tourism.setText(date_tiket);
        } else {
            name_tourism.setVisibility(View.GONE);
            txt_tourism.setVisibility(View.GONE);
            date_tourism.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
        }

        if (homestay != null) {
            name_homestay.setText(homestay);
            date_homestay.setText(check_in + " sampai " + check_out);
        } else {
            name_homestay.setVisibility(View.GONE);
            txt_homestay.setVisibility(View.GONE);
            date_homestay.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
        }
        bSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSurvey();
            }
        });

        getUserReview();
    }

    private void getUserReview() {
        String token = "bearer" + Hawk.get(Constant.TOKEN, "");
        Api.getService().userReview(token, id_transaksi).enqueue(new Callback<UserReviewRequest>() {
            @Override
            public void onResponse(Call<UserReviewRequest> call, Response<UserReviewRequest> response) {
                if (response.isSuccessful()) {
                    UserReviewRequest userReviewRequest = response.body();
                    if (userReviewRequest.getUserReview().getStatus().equalsIgnoreCase("belum terjawab")) {
                        bSurvey.setVisibility(View.VISIBLE);
                    } else {
                        bSurvey.setVisibility(View.INVISIBLE);
                    }
                }

            }

            @Override
            public void onFailure(Call<UserReviewRequest> call, Throwable t) {

            }
        });
    }

    private void getSurvey() {
        String token = "bearer" + Hawk.get(Constant.TOKEN, "");
        Api.getService().getSurvey(token, id_menu).enqueue(new Callback<SurveyRequest>() {
            @Override
            public void onResponse(Call<SurveyRequest> call, Response<SurveyRequest> response) {
                if (response.isSuccessful()) {
                    SurveyRequest surveyRequest = response.body();
                    final ArrayList<Survey> surveys = surveyRequest.getSurveys().getSurvey();
                    ArrayList<pertanyaan> pertanyaans = null;
                    pertanyaans = surveys.get(0).getPertanyaans();
                    sendSurvey(pertanyaans);
                }
            }

            @Override
            public void onFailure(Call<SurveyRequest> call, Throwable t) {

            }
        });
    }

    private void sendSurvey(ArrayList<pertanyaan> pertanyaans) {
        Intent intent = new Intent(getApplicationContext(), PertanyaanActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("pertanyaan", pertanyaans);
        intent.putExtra("bundle", args);
        intent.putExtra("transaction_id", id_transaksi);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

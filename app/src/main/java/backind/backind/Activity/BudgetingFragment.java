package backind.backind.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.opengl.Visibility;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.text.NumberFormat;
import java.util.Locale;

import backind.backind.R;
import backind.backind.Response.BudgetingResponse;
import backind.backind.Service.Api;
import backind.backind._sliders.LoosyDialog;
import backind.backind._sliders.SortBottomSheetDialog;
import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BudgetingFragment extends Fragment implements View.OnClickListener {
    private EditText jarak, review, budget, polling, minvalue, maxvalue;
    private Button save,sad,netral,smile,happy;
    private TextView loosy,ketLoosy;
    private RadioGroup radioGroup;
    private RadioButton rbyes,rbno;

    AlertDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView =  inflater.inflate(R.layout.fragment_budgeting, container, false);
//        jarak = rootView.findViewById(R.id.jarak);
        review = rootView.findViewById(R.id.review);
        budget = rootView.findViewById(R.id.budget);
        loosy = rootView.findViewById(R.id.tv_loosy);
        ketLoosy = rootView.findViewById(R.id.ket_loosy);
//        polling = rootView.findViewById(R.id.polling);
//        minvalue = rootView.findViewById(R.id.rangemin);
//        maxvalue = rootView.findViewById(R.id.rangemax);
        save = rootView.findViewById(R.id.save);
        radioGroup = rootView.findViewById(R.id.radio_grup);
//        rangeSeekBar = rootView.findViewById(R.id.rangebar);
        rbyes = rootView.findViewById(R.id.radio_yes);
        rbno = rootView.findViewById(R.id.radio_no);
//        smile = rootView.findViewById(R.id.smile);
//        happy = rootView.findViewById(R.id.happy);

        rbyes.setOnClickListener(this);
        rbno.setOnClickListener(this);
//        smile.setOnClickListener(this);
//        happy.setOnClickListener(this);

//        rangeSeekBar.setRangeValues(0,15000);
//        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
//            @Override
//            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
//                Log.e("value",minValue+" "+maxValue);
//                minvalue.setText(NumberFormat.getNumberInstance(Locale.US).format(minValue));
//                maxvalue.setText(maxValue.toString());
//            }
//        });


        loosy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (budget.getText().toString().isEmpty()) {
                    budget.setError("Harus diisi");
                } else {
                    Intent intent = new Intent(getContext(),CatalogActivity.class);
                    intent.putExtra("budget", budget.getText().toString());
                    intent.putExtra("loosy", ketLoosy.getText().toString());
                    startActivity(intent);
                }

            }
        });
        return rootView;
    }


    public void openDialog(){
        LoosyDialog loosyDialog = new LoosyDialog();
        loosyDialog.show(getFragmentManager(),"loosy dialog");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.radio_yes:
                ketLoosy.setText("&loosy=true");
                break;
            case R.id.radio_no:
                ketLoosy.setText("");
                break;
        }
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.sad:
//                review.setText("1");
//                Toast.makeText(getContext(),"Review Sad",Toast.LENGTH_LONG).show();
//                break;
//            case R.id.netral:
//                review.setText("2");
//                Toast.makeText(getContext(),"Review Netral",Toast.LENGTH_LONG).show();
//                break;
//            case R.id.smile:
//                review.setText("3");
//                Toast.makeText(getContext(),"Review Smile",Toast.LENGTH_LONG).show();
//                break;
//            case R.id.happy:
//                review.setText("4");
//                Toast.makeText(getContext(),"Review Happy",Toast.LENGTH_LONG).show();
//                break;
//        }
//    }

//    public void checkButton(View v){
//        int radioId = radioGroup.getCheckedRadioButtonId();
//        radioButton = radioButton.findViewById(radioId);
//        Toast.makeText(getContext(),"Review"+radioButton.getText(),Toast.LENGTH_LONG).show();
//    }
}

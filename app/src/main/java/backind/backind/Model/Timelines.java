package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Timelines {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("id_pengaduan")
    @Expose
    private Integer id_pengaduan;
    @SerializedName("id_assignee")
    @Expose
    private Integer id_assignee;
    @SerializedName("id_assigned_by")
    @Expose
    private Integer id_assigned_by;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("users")
    @Expose
    private User user;
    @SerializedName("user1")
    @Expose
    private User user1;
    @SerializedName("pengaduan")
    @Expose
    private Pengaduan pengaduan;
    @SerializedName("status_user")
    @Expose
    private String status_user;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_pengaduan() {
        return id_pengaduan;
    }

    public void setId_pengaduan(Integer id_pengaduan) {
        this.id_pengaduan = id_pengaduan;
    }

    public Integer getId_assignee() {
        return id_assignee;
    }

    public void setId_assignee(Integer id_assignee) {
        this.id_assignee = id_assignee;
    }

    public Integer getId_assigned_by() {
        return id_assigned_by;
    }

    public void setId_assigned_by(Integer id_assigned_by) {
        this.id_assigned_by = id_assigned_by;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public Pengaduan getPengaduan() {
        return pengaduan;
    }

    public void setPengaduan(Pengaduan pengaduan) {
        this.pengaduan = pengaduan;
    }

    public String getStatus_user() {
        return status_user;
    }

    public void setStatus_user(String status_user) {
        this.status_user = status_user;
    }
}

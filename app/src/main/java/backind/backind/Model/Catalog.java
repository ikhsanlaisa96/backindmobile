package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Catalog {
    @SerializedName("groups")
    @Expose
    private List<Group> groups;
    @SerializedName("data")
    @Expose
    private Object data;
    @SerializedName("input")
    @Expose
    private Input input;
    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public static class Group implements Serializable
    {

        @SerializedName("id_business")
        @Expose
        private Integer idBusiness;
        @SerializedName("id_menu")
        @Expose
        private Integer idMenu;
        @SerializedName("id_user")
        @Expose
        private Integer idUser;
        @SerializedName("id_city")
        @Expose
        private Integer idCity;
        @SerializedName("business_status")
        @Expose
        private Integer businessStatus;
        @SerializedName("id_business_details")
        @Expose
        private Integer idBusinessDetails;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("reviews")
        @Expose
        private Integer reviews;
        @SerializedName("review_rating")
        @Expose
        private String reviewRating;
        @SerializedName("city")
        @Expose
        private City city;
        @SerializedName("business_details")
        @Expose
        private BusinessDetails businessDetails;
        private final static long serialVersionUID = 1821849312213318872L;

        public Integer getIdBusiness() {
            return idBusiness;
        }

        public void setIdBusiness(Integer idBusiness) {
            this.idBusiness = idBusiness;
        }

        public Integer getIdMenu() {
            return idMenu;
        }

        public void setIdMenu(Integer idMenu) {
            this.idMenu = idMenu;
        }

        public Integer getIdUser() {
            return idUser;
        }

        public void setIdUser(Integer idUser) {
            this.idUser = idUser;
        }

        public Integer getIdCity() {
            return idCity;
        }

        public void setIdCity(Integer idCity) {
            this.idCity = idCity;
        }

        public Integer getBusinessStatus() {
            return businessStatus;
        }

        public void setBusinessStatus(Integer businessStatus) {
            this.businessStatus = businessStatus;
        }

        public Integer getIdBusinessDetails() {
            return idBusinessDetails;
        }

        public void setIdBusinessDetails(Integer idBusinessDetails) {
            this.idBusinessDetails = idBusinessDetails;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getReviews() {
            return reviews;
        }

        public void setReviews(Integer reviews) {
            this.reviews = reviews;
        }

        public String getReviewRating() {
            return reviewRating;
        }

        public void setReviewRating(String reviewRating) {
            this.reviewRating = reviewRating;
        }

        public City getCity() {
            return city;
        }

        public void setCity(City city) {
            this.city = city;
        }

        public BusinessDetails getBusinessDetails() {
            return businessDetails;
        }

        public void setBusinessDetails(BusinessDetails businessDetails) {
            this.businessDetails = businessDetails;
        }

    }

    public static class BusinessDetails implements Serializable
    {

        @SerializedName("id_business_details")
        @Expose
        private Integer idBusinessDetails;
        @SerializedName("business_name")
        @Expose
        private String businessName;
        @SerializedName("business_email")
        @Expose
        private String businessEmail;
        @SerializedName("business_address")
        @Expose
        private String businessAddress;
        @SerializedName("business_lat")
        @Expose
        private String businessLat;
        @SerializedName("business_lang")
        @Expose
        private String businessLang;
        @SerializedName("business_phone")
        @Expose
        private String businessPhone;
        @SerializedName("business_open_time")
        @Expose
        private String businessOpenTime;
        @SerializedName("business_close_time")
        @Expose
        private String businessCloseTime;
        @SerializedName("business_price")
        @Expose
        private Integer businessPrice;
        @SerializedName("business_desc")
        @Expose
        private String businessDesc;
        @SerializedName("business_profile_pict")
        @Expose
        private String businessProfilePict;
        @SerializedName("condition")
        @Expose
        private String condition;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        private final static long serialVersionUID = 8858477519252282147L;

        public Integer getIdBusinessDetails() {
            return idBusinessDetails;
        }

        public void setIdBusinessDetails(Integer idBusinessDetails) {
            this.idBusinessDetails = idBusinessDetails;
        }

        public String getBusinessName() {
            return businessName;
        }

        public void setBusinessName(String businessName) {
            this.businessName = businessName;
        }

        public String getBusinessEmail() {
            return businessEmail;
        }

        public void setBusinessEmail(String businessEmail) {
            this.businessEmail = businessEmail;
        }

        public String getBusinessAddress() {
            return businessAddress;
        }

        public void setBusinessAddress(String businessAddress) {
            this.businessAddress = businessAddress;
        }

        public String getBusinessLat() {
            return businessLat;
        }

        public void setBusinessLat(String businessLat) {
            this.businessLat = businessLat;
        }

        public String getBusinessLang() {
            return businessLang;
        }

        public void setBusinessLang(String businessLang) {
            this.businessLang = businessLang;
        }

        public String getBusinessPhone() {
            return businessPhone;
        }

        public void setBusinessPhone(String businessPhone) {
            this.businessPhone = businessPhone;
        }

        public String getBusinessOpenTime() {
            return businessOpenTime;
        }

        public void setBusinessOpenTime(String businessOpenTime) {
            this.businessOpenTime = businessOpenTime;
        }

        public String getBusinessCloseTime() {
            return businessCloseTime;
        }

        public void setBusinessCloseTime(String businessCloseTime) {
            this.businessCloseTime = businessCloseTime;
        }

        public Integer getBusinessPrice() {
            return businessPrice;
        }

        public void setBusinessPrice(Integer businessPrice) {
            this.businessPrice = businessPrice;
        }

        public String getBusinessDesc() {
            return businessDesc;
        }

        public void setBusinessDesc(String businessDesc) {
            this.businessDesc = businessDesc;
        }

        public String getBusinessProfilePict() {
            return businessProfilePict;
        }

        public void setBusinessProfilePict(String businessProfilePict) {
            this.businessProfilePict = businessProfilePict;
        }

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public static class City implements Serializable
    {

        @SerializedName("id_city")
        @Expose
        private Integer idCity;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("pict")
        @Expose
        private Object pict;
        private final static long serialVersionUID = -7173114816578983035L;

        public Integer getIdCity() {
            return idCity;
        }

        public void setIdCity(Integer idCity) {
            this.idCity = idCity;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getPict() {
            return pict;
        }

        public void setPict(Object pict) {
            this.pict = pict;
        }

    }

    public static class Input implements Serializable
    {

        @SerializedName("amount")
        @Expose
        private String amount;
        private final static long serialVersionUID = -47751420580483401L;

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

    }
}

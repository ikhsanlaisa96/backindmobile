package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DataLayanan {

    @SerializedName("layanan")
    @Expose
    private ArrayList<Layanan> layanan = null;

    public ArrayList<Layanan> getLayanan() {
        return layanan;
    }

    public void setLayanan(ArrayList<Layanan> layanan) {
        this.layanan = layanan;
    }
}

package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Pengaduan {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("id_siswa")
    @Expose
    private Integer idSiswa;
    @SerializedName("id_jenis")
    @Expose
    private Integer idJenis;
    @SerializedName("isi")
    @Expose
    private String isi;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("hasil")
    @Expose
    private String hasil;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status")
    @Expose
    private ArrayList<Timeline> status = null;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("layanan")
    @Expose
    private Layanan layanan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdSiswa() {
        return idSiswa;
    }

    public void setIdSiswa(Integer idSiswa) {
        this.idSiswa = idSiswa;
    }

    public Integer getIdJenis() {
        return idJenis;
    }

    public void setIdJenis(Integer idJenis) {
        this.idJenis = idJenis;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getHasil() {
        return hasil;
    }

    public void setHasil(String hasil) {
        this.hasil = hasil;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

//    public ArrayList<Timeline> getStatus() {
//        return status;
//    }
//
//    public void setStatus(ArrayList<Timeline> status) {
//        this.status = status;
//    }


    public ArrayList<Timeline> getStatus() {
        return status;
    }

    public void setStatus(ArrayList<Timeline> status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Layanan getLayanan() {
        return layanan;
    }

    public void setLayanan(Layanan layanan) {
        this.layanan = layanan;
    }
}

package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DataTimeline {
    @SerializedName("timeline")
    @Expose
    private ArrayList<Timelines> timeliness= null;

    public ArrayList<Timelines> getTimeliness() {
        return timeliness;
    }

    public void setTimeliness(ArrayList<Timelines> timeliness) {
        this.timeliness = timeliness;
    }
}

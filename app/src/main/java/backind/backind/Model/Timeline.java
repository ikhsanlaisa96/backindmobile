package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Timeline implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("id_pengaduan")
    @Expose
    private Integer id_pengaduan;
    @SerializedName("id_assignee")
    @Expose
    private Integer id_assignee;
    @SerializedName("id_assigned_by")
    @Expose
    private Integer id_assigned_by;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("id_user")
    @Expose
    private Integer id_user;
    @SerializedName("name")
    @Expose
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_pengaduan() {
        return id_pengaduan;
    }

    public void setId_pengaduan(Integer id_pengaduan) {
        this.id_pengaduan = id_pengaduan;
    }

    public Integer getId_assignee() {
        return id_assignee;
    }

    public void setId_assignee(Integer id_assignee) {
        this.id_assignee = id_assignee;
    }

    public Integer getId_assigned_by() {
        return id_assigned_by;
    }

    public void setId_assigned_by(Integer id_assigned_by) {
        this.id_assigned_by = id_assigned_by;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Surveys implements Serializable {
    @SerializedName("surveys")
    @Expose
    private ArrayList<Survey> survey = null;

    public ArrayList<Survey> getSurvey() {
        return survey;
    }

    public void setSurvey(ArrayList<Survey> survey) {
        this.survey = survey;
    }
}

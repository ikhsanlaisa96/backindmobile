package backind.backind.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DataPengaduan {
    @SerializedName("pengaduan")
    @Expose
    private ArrayList<Pengaduan> pengaduan = null;

    public ArrayList<Pengaduan> getPengaduan() {
        return pengaduan;
    }

    public void setPengaduan(ArrayList<Pengaduan> pengaduan) {
        this.pengaduan = pengaduan;
    }
}

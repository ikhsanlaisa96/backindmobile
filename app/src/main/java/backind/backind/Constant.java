package backind.backind;

public class Constant {
    public static final String BASE_URL = "http://backind.com/api/";
    public static final String DATA = "data";
    public static final String USER = "user";
    public static final String ROLE = "role";
    public static final String TOKEN = "jwt_token";
    public static final Integer ID_USER = 0;
    public static final String TAG_SUCCESS = "Success";
    public static final String TAG_NETWORK = "Please Check your Internet Connection";
    public static final String BASE_URL_PHOTO = "http://backind.com/storage/";
    public static final String STATUS_LOGIN = "statuslogin";
    public static final String API_CATALOG = "http://34.229.46.199:8081/api/catalog/groups?amount=";

    public static class DataLocal {
        public static final String dataUser = "profiledata";
    }
}

package backind.backind.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BudgetingResponse {
    @SerializedName("nominal")
    @Expose
    String nominal;

    @SerializedName("polling")
    @Expose
    String polling;

    @SerializedName("review")
    @Expose
    String review;

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getPolling() {
        return polling;
    }

    public void setPolling(String polling) {
        this.polling = polling;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}

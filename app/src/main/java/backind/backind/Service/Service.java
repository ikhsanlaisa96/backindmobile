package backind.backind.Service;

import backind.backind.Model.AddPengaduanRequest;
import backind.backind.Model.Catalog;
import backind.backind.Model.LayananRequest;
import backind.backind.Model.PaymentReceipt;
import backind.backind.Model.PengaduanRequest;
import backind.backind.Model.Request;
import backind.backind.Model.SurveyRequest;
import backind.backind.Model.TimelineRequest;
import backind.backind.Model.UserReview;
import backind.backind.Model.UserReviewRequest;
import backind.backind.Response.BaseResponse;
import backind.backind.Response.BudgetingResponse;
import backind.backind.Response.BusinessDetailsResponse;
import backind.backind.Response.BusinessResponse;
import backind.backind.Response.EticketResponse;
import backind.backind.Response.InvoiceResponse;
import backind.backind.Response.NearbyResponse;
import backind.backind.Response.CityResponse;
import backind.backind.Response.LoginResponse;
import backind.backind.Response.ProfileResponse;
import backind.backind.Response.RegisterResponse;
import backind.backind.Response.ReviewResponse;
import backind.backind.Response.TransaksiResponse;
import backind.backind.Response.UpdateCostResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface Service {
    @FormUrlEncoded
    @POST("register")
    Call<RegisterResponse> register(@Field("name") String name, @Field("email") String email, @Field("phone_number") String phone_number, @Field("password") String password);

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("postUpdateDataUser")
    Call<ProfileResponse> updateUser(@Field("name") String name, @Field("email") String email, @Field("phone_number") String phone_number);

    @GET("getMinCity")
    Call<CityResponse> getListCity();

    @GET("getTicketHistory")
    Call<EticketResponse> getTicketList();

    @FormUrlEncoded
    @POST()
    Call<BusinessResponse> getDataTourism(@Url String url, @Field("keysearch") String keysearch);

    //    @GET("getMinCity")
//    Call<BusinessResponse> getDataTourism();
    @FormUrlEncoded
    @POST()
    Call<BusinessResponse> getDataHomestay(@Url String url, @Field("keysearch") String keysearch);

    @FormUrlEncoded
    @POST()
    Call<BusinessResponse> getFilterHomestay(@Url String url, @Field("minprice") int minprize, @Field("maxprice") int maxprize, @Field("avg_review") int avg_review);

    @FormUrlEncoded
    @POST()
    Call<BusinessResponse> getFilterTourism(@Url String url, @Field("minprice") int minprize, @Field("maxprice") int maxprize, @Field("avg_review") int avg_review);

    @FormUrlEncoded
    @POST()
    Call<BusinessResponse> getResetFilterHomestay(@Url String url, @Field("minprice") int minprize, @Field("maxprice") int maxprize, @Field("avg_review") int avg_review, @Field("reset") String reset);

    @FormUrlEncoded
    @POST()
    Call<BusinessResponse> gettResetFilterTourism(@Url String url, @Field("minprice") int minprize, @Field("maxprice") int maxprize, @Field("avg_review") int avg_review, @Field("reset") String reset) ;

//    @FormUrlEncoded
//    @POST()
//    Call<BusinessResponse> getDataTourism(@Url String url, @Field("keysearch") String keysearch);
//
//    //    @GET("getMinCity")
////    Call<BusinessResponse> getDataTourism();
//    @FormUrlEncoded
//    @POST()
//    Call<BusinessResponse> getDataHomestay(@Url String url, @Field("keysearch") String keysearch);

    @FormUrlEncoded
    @POST()
    Call<NearbyResponse> getNearby(@Url String url, @Field("business_price") int price, @Field("business_name") String business_name, @Field("id_user") Integer iduser, @Field("email") String email);

    @FormUrlEncoded
    @POST()
    Call<BusinessDetailsResponse> getDetailPerBusiness(@Url String url, @Field("business_name") String business_name, @Field("id_user") Integer iduser, @Field("email") String email);

    @GET()
    Call<InvoiceResponse> getTransaction(@Url String url);

    @FormUrlEncoded
    @POST
    Call<UpdateCostResponse> getUpdateCost(@Url String url, @Field("total_cost") int total_cost, @Field("id_menu") int id_menu);

    @FormUrlEncoded
    @POST
    Call<ReviewResponse> postReview(@Url String url, @Field("id_business") int id_business, @Field("review") String review, @Field("response") String response, @Field("rating") int rating, @Field("id_user") int id_user);


    @POST
    @Multipart
    Call<PaymentReceipt> paymentReceipt(@Url String url, @Part("id_transaksi") RequestBody id_transaksi, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("postAddBooking")
    Call<TransaksiResponse> booking(
            @Field("tourism") int id_tourism,
            @Field("homestay") int id_homestay,
            @Field("checkin") String checkin,
            @Field("checkout") String checkout,
            @Field("checkin_tourism") String checkin_tourism,
            @Field("total_ticket") int total_ticket);

    @FormUrlEncoded
    @POST("updatePassword")
    Call<BaseResponse> updatePassword(@Field("password") String password);

    @FormUrlEncoded
    @POST("getTimeline")
    Call<TimelineRequest> getTimeline(@Field("id_pengaduan") Integer id_pengaduan);

    @FormUrlEncoded
    @POST("sendMessage")
    Call<BaseResponse> sendMessage(@Field("id_pengaduan") Integer id_pengaduan, @Field("notes") String notes);

    @FormUrlEncoded
    @POST("getSurvey")
    Call<SurveyRequest> getSurvey(@Header("Authorization") String token, @Field("id_menu") Integer id_menu);

    @GET("pengaduanbyuser")
    Call<PengaduanRequest> getPengaduan();

    @GET("getLayanan")
    Call<LayananRequest> getLayanan();

    @POST("addpengaduan")
    @Multipart
    Call<AddPengaduanRequest> addPengaduan(@Part("id_jenis") int idJenis, @Part("isi") RequestBody isi,
                                           @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("submitsurvey")
    Call<Request> submitSurvey(@Header("Authorization") String token, @Field("transaction_id") int transaction_id, @Field("id_pertanyaan") String idPertanyaan,
                               @Field("jawaban") String jawaban);

    @FormUrlEncoded
    @POST("userReview")
    Call<UserReviewRequest> userReview(@Header("Authorization") String token, @Field("transaction_id") int transaction_id);

    @FormUrlEncoded
    @POST("user-preferences")
    Call<BudgetingResponse> addBudgeting(@Field("nominal") String nominal, @Field("polling") String polling, @Field("review") String review);


}

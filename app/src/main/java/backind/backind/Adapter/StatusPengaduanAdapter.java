package backind.backind.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import backind.backind.Model.Timeline;
import backind.backind.R;

public class StatusPengaduanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<Timeline> items;
    OnItemClickListener listener;

    public StatusPengaduanAdapter(Context context, ArrayList<Timeline> items, OnItemClickListener listener) {
        this.context=context;
        this.items= items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View row=inflater.inflate(R.layout.status_pengaduan_item, viewGroup, false);
        Item item=new Item(row);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Item holder = new Item(viewHolder.itemView);
        //((Item)viewHolder).textView.setText(items[position]);
        final Timeline statusPengaduan = getItem(position);

//        Toast.makeText(context, "ini namaenya" + statusPengaduan, Toast.LENGTH_LONG).show();
        holder.progresTxt.setText(statusPengaduan.getStatus());
        if(statusPengaduan.getCreatedAt()==null) {
            Picasso.get()
                    .load(R.drawable.tidak_terceklis)
                    .into(holder.imageView);
            holder.tanggalTxt.setText("Menunggu penanganan admin");
        }else{
            holder.tanggalTxt.setText(statusPengaduan.getCreatedAt());
//            holder.assignee.setText(statusPengaduan.getName());
        }
        if(position==items.size()-1){
            holder.timelineTxt.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(statusPengaduan);
            }
        });

    }

    public Timeline getItem(int position){
        return items.get(position);
    }
    public interface OnItemClickListener {
        void onItemClick(Timeline item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class Item extends RecyclerView.ViewHolder {
        TextView progresTxt, tanggalTxt, timelineTxt, assignee;
        ImageView imageView;
        public Item(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.status_progres_pengaduan);
            progresTxt= (TextView) itemView.findViewById(R.id.status_text);
            tanggalTxt= (TextView) itemView.findViewById(R.id.status_waktu);
            timelineTxt= (TextView) itemView.findViewById(R.id.timeline);
//            assignee = itemView.findViewById(R.id.assignee);
        }
    }
}

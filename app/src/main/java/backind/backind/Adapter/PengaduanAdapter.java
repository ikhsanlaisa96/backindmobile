package backind.backind.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import backind.backind.Constant;
import backind.backind.Model.Pengaduan;
import backind.backind.Model.Timeline;
import backind.backind.R;

public class PengaduanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<Pengaduan> items;
    OnItemClickListener listener;

    public PengaduanAdapter(Context context, ArrayList<Pengaduan> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.pengaduan_item, viewGroup, false);
        Item item = new Item(row);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Item holder = new Item(viewHolder.itemView);
        final Pengaduan pengaduan = getItem(position);
        holder.judulTxt.setText("Ini Judul Pengaduan");
        holder.isiTxt.setText(pengaduan.getIsi());
        holder.kategoriTxt.setText(pengaduan.getLayanan().getJenis());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(pengaduan);
            }
        });
        ArrayList<Timeline> status = pengaduan.getStatus();
        Log.d("PENGADUAN:", "" + (pengaduan.getStatus()));
        Timeline lastStatus = null;
        for (Timeline st : status) {
            Log.d("STATUS : ",st.getStatus()+" "+st.getCreatedAt());
//            lastStatus = st;
            if (st.getCreatedAt()!= null)
                lastStatus = st;
//            }
        }
        holder.statusTxt.setText(lastStatus.getStatus());
        Log.d("HAHAHHAHAHA", lastStatus.getStatus());
        Log.d("HAHAHHAHAHAs", String.valueOf(lastStatus.getId()));
        if (lastStatus.getStatus().equalsIgnoreCase("NEW")) {
            holder.statusTxt.setBackgroundResource(R.drawable.rounded_status_submit);
        } else if (lastStatus.getStatus().equalsIgnoreCase("ASSIGNED")) {
            holder.statusTxt.setBackgroundResource(R.drawable.rounded_status_progress);
        } else {
            holder.statusTxt.setBackgroundResource(R.drawable.rounded_status_selesai);
        }
        Log.d("Foto", Constant.BASE_URL_PHOTO + items.get(position).getFoto());
        Picasso.get()
                .load(Constant.BASE_URL_PHOTO + items.get(position).getFoto())
                .placeholder(R.drawable.icon_backpacker)
                .into(holder.imageView);
    }

    public Pengaduan getItem(int position) {
        return items.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(Pengaduan item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class Item extends RecyclerView.ViewHolder {
        TextView judulTxt, isiTxt, kategoriTxt, statusTxt;
        ImageView imageView;

        public Item(@NonNull View itemView) {
            super(itemView);
            judulTxt = (TextView) itemView.findViewById(R.id.pengaduan_pre_title);
            kategoriTxt = (TextView) itemView.findViewById(R.id.pengaduan_pre_kategori);
            isiTxt = (TextView) itemView.findViewById(R.id.pengaduan_pre_isi);
            imageView = (ImageView) itemView.findViewById(R.id.pengaduan_pre_pict);
            statusTxt = (TextView) itemView.findViewById(R.id.pengaduan_status);
        }
    }

}


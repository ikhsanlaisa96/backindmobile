package backind.backind.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import backind.backind.Model.Opsi;
import backind.backind.Model.pertanyaan;
import backind.backind.R;

public class SurveyAdapter extends PagerAdapter {
    private ArrayList<pertanyaan> items;
    private Context context;
    OnItemClickListener listener;
    TextView pertanyaanTxt;
    LinearLayout pilihanLayout, isianLayout;
    EditText isianTxt;
    ArrayList<Integer> checkedIds;
    ArrayList<String> answerStrings;

    public SurveyAdapter(Context context, ArrayList<pertanyaan> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
        checkedIds = new ArrayList<>();
        answerStrings = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            checkedIds.add(i, null);
            answerStrings.add(i, null);
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public Integer getJawaban(int position) {
        return checkedIds.get(position);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public ArrayList<Integer> getCheckedIds() {
        return checkedIds;
    }

    public ArrayList<String> getAnswerStrings() {
        return answerStrings;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.pertanyaan_item, container,
                false);
        pertanyaanTxt = (TextView) layout.findViewById(R.id.pertanyaan);
        pilihanLayout = (LinearLayout) layout.findViewById(R.id.pertanyaan_pilihan);
        isianLayout = (LinearLayout) layout.findViewById(R.id.pertanyaan_isian);
        isianTxt = (EditText) layout.findViewById(R.id.isian_pertanyaan);
        final pertanyaan pertanyaan = items.get(position);
        pertanyaanTxt.setText((position + 1) + ". " + pertanyaan.getPertanyaan());
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(pertanyaan);
            }
        });
        pilihanLayout.removeAllViews();
        pilihanLayout.setVisibility(View.VISIBLE);
        isianLayout.setVisibility(View.GONE);
        RadioGroup rg = new RadioGroup(context);
        rg.setOrientation(RadioGroup.VERTICAL);
        // Initialize the layout parameters for RadioGroup
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rg.setLayoutParams(lp);
        ArrayList<Opsi> opsi = pertanyaan.getOpsi();
        for (Opsi o : opsi) {
            RadioButton newRadio = new RadioButton(context);
            newRadio.setId(o.getId());
            newRadio.setText(o.getJawaban());
            newRadio.setTextColor(Color.parseColor("#262626"));
            newRadio.setTextSize(12);
            rg.addView(newRadio);
        }
        if (checkedIds.get(position) != null) {
            //Toast.makeText(context, checkedIds.get(position), Toast.LENGTH_SHORT).show();
            rg.check(checkedIds.get(position));
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkedIds.set(position, checkedId);
                Log.d("CHECK", "dipilih : " + checkedId);
            }
        });

        pilihanLayout.addView(rg);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public interface OnItemClickListener {
        void onItemClick(pertanyaan item);
    }
}

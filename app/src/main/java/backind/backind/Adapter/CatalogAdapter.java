package backind.backind.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import backind.backind.Model.Catalog;
import backind.backind.R;

public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<Catalog.Group> dataModelArrayList;
    Context mContext;

    public CatalogAdapter(Context ctx, ArrayList<Catalog.Group> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.dataModelArrayList = dataModelArrayList;
        this.mContext = ctx;
    }

    @Override
    public CatalogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_catalog, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(CatalogAdapter.ViewHolder holder, int position) {

        holder.nama.setText(dataModelArrayList.get(position).getBusinessDetails().getBusinessName());
        holder.almat.setText(dataModelArrayList.get(position).getBusinessDetails().getBusinessAddress());
        holder.hrga.setText("Rp. "+ String.valueOf(dataModelArrayList.get(position).getBusinessDetails().getBusinessPrice()));
//        Glide.with(mContext).load(dataModelArrayList.get(position).getBusinessDetails().getBusinessProfilePict()).into(holder.iv);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView almat, nama, hrga;
        ImageView iv;

        public ViewHolder(View itemView) {
            super(itemView);

            almat = (TextView) itemView.findViewById(R.id.alamatTempat);
            nama = (TextView) itemView.findViewById(R.id.namaTempat);
            hrga = (TextView) itemView.findViewById(R.id.price_tempat);
            iv = (ImageView) itemView.findViewById(R.id.iv);
        }

    }
}

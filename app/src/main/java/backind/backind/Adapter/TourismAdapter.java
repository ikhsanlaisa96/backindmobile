package backind.backind.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.enums.Position;

import java.util.ArrayList;
import java.util.List;

import backind.backind.Activity.NearbyActivity;
import backind.backind.Constant;
import backind.backind.Model.BusinessData;
import backind.backind.Model.BusinessDetails;
import backind.backind.Model.Review;
import backind.backind.R;

public class TourismAdapter extends RecyclerView.Adapter<TourismAdapter.MyViewHolder> {

    private int hargaSearch = 5000000;
    private Context mContext;
    private List<BusinessData> tourismList = new ArrayList<>();
    private List<Review> reviewData = null;
    List<BusinessData> bisnisList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tourism, startFrom, number_reviews;
        public ImageView thumbnail;
        public RatingBar totalStar;

        public MyViewHolder(View view) {
            super(view);
            tourism =  view.findViewById(R.id.bisnis);
            startFrom =  view.findViewById(R.id.harga);
            thumbnail =  view.findViewById(R.id.img);
            totalStar = view.findViewById(R.id.total_star);
            number_reviews = view.findViewById(R.id.number_reviews);

            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int mPosition = getLayoutPosition();
                    //Toast.makeText(mContext,"Di klik id = " + tourismList.get(mPosition).getIdBusinessDetails().toString(),Toast.LENGTH_LONG).show();
                    //Toast.makeText(mContext,"Di klik harga search = " + hargaSearch,Toast.LENGTH_LONG).show();

                    String element = tourismList.get(mPosition).toString();
                    Toast.makeText(view.getContext(), tourism.getText(), Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(view.getContext(), NearbyActivity.class);
                    int idmenu = Integer.parseInt(tourismList.get(mPosition).getIdMenu());
                    i.putExtra("id_menu", idmenu);
                    i.putExtra("id_bisnis", Integer.parseInt(tourismList.get(mPosition).getIdBusinessDetails().toString()));
                    i.putExtra("business_name", tourismList.get(mPosition).getBusinessDetails().getBusinessName());
                    i.putExtra("harga_search", hargaSearch);
                    view.getContext().startActivity(i);
                }
            });
        }
    }


    public TourismAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItems(List<BusinessData> business){
        this.tourismList.addAll(business);
        notifyDataSetChanged();

    }

    public void setFilter(int hargaSearch, List<BusinessData> business){
        this.hargaSearch = hargaSearch;
        this.tourismList = new ArrayList<BusinessData>();
        this.tourismList.addAll(business);
        super.notifyDataSetChanged();

    }

    public void setFilters(List<BusinessData> business){
        this.tourismList = new ArrayList<>();
        this.tourismList.addAll(business);
        notifyDataSetChanged();
    }


    @Override
    public TourismAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bisnis_card, parent, false);

        return new TourismAdapter.MyViewHolder(itemView);
    }

    public void updatedata(List<BusinessData> business){
        this.tourismList.clear();
        this.tourismList.addAll(business);
        notifyDataSetChanged();
    }

    int n;
    @Override
    public void onBindViewHolder(final TourismAdapter.MyViewHolder holder, int position) {
        BusinessDetails album = tourismList.get(position).getBusinessDetails();
        List<Review> review = tourismList.get(position).getReviews();
        n = Integer.valueOf(album.getIdBusinessDetails());
        holder.tourism.setText(album.getBusinessName());
        holder.startFrom.setText("Rp " + album.getBusinessPrice() + ",-");
        String n_review = String.valueOf(review.size());
        reviewData = new ArrayList<Review>();
        reviewData = review;
        holder.number_reviews.setText(reviewData.size() + " reviews");
        float sumRate = 0;
        for (Review reviews : reviewData) {
            sumRate += Integer.valueOf(reviews.getRating());
            Log.d("Backindbug", reviews.getRating());
        }
        float avgRate = sumRate / reviewData.size();
        Log.d("iniavgnya", sumRate + "");
        //Toast.makeText(BusinessDetailActivity.this, avgRate + "", Toast.LENGTH_SHORT).show();
        holder.totalStar.setRating(tourismList.get(position).getAvg_review());

        // loading album cover using Glide library
        Glide.with(mContext).load(Constant.BASE_URL_PHOTO +album.getBusinessProfilePict()).into(holder.thumbnail);
//        Picasso.get().load(Constant.BASE_URL_PHOTO + album.getBusinessProfilePict().;
    }


    @Override
    public int getItemCount() {
        return tourismList.size();
    }

}
